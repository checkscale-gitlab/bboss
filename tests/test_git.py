'''
Created on Nov 27, 2018

@author: laurent
'''

import os
import git

def generate(projectname):
    # Check we are not already inside a git repository
    try:
        repo = git.Repo(search_parent_directories=True)
    except  git.InvalidGitRepositoryError:
        repo = None

    if repo is not None:
        print("[Error] Unable to generate a project inside an existing repository")
        return

    # Check the repository does not already exist
    if os.path.exists(projectname):
        print("[Error] A directory named '%s' already exists" % projectname)
        return

    # Create the repository
    repo_obj = git.Repo.init(projectname)
    
    # def add(cls, repo, name, path, url=None, branch=None, no_checkout=False):
    
    repo_url = "https://gitlab.com/bboss-org/bboss-distrib.git"
    repo_url = "https://github.com/floross/docker-readthedocs.git"
    repo_obj.create_submodule(".distrib", ".distrib", repo_url)

if __name__ == "__main__":
    generate("toto")
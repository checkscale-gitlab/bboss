#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# Minimal parameter to run the script
[[ -n "${!BUILD_*}" ]] && unset "${!BUILD_*}"
export BUILD_SCT_PRODUCT='bboss'

# Clean output
./build.sh "${@}" -c install
vagrant global-status --prune
rm -rf output

export BBOSS_ENV_DISTRIB_BRANCH='develop'

# Specify number to serialize the parallelization and run all values even
# some of them failed.
export BUILD_SCT_RUNINDEX=1
export BUILD_SCT_RUNTOTAL=6
export BUILD_SCT_LOGPATH
export INSTALL_SCT_GETURL='../../install.sh'
export INSTALL_SCT_PRODURL='git+http://gitlab.com/bboss-org/bboss.git@develop#egg=bboss'
# export INSTALL_SCT_PRODURL='git+http://gitlab.bbossci.nuci3.local/bboss-org/bboss.git@develop#egg=bboss'

tst_lst=$(eval echo "{1..${BUILD_SCT_RUNTOTAL}}")
for i in ${tst_lst}; do 
  BUILD_SCT_RUNINDEX=${i}
  BUILD_SCT_LOGPATH=$(printf 'output/logs~%0.2d/' "${i}")
  ./build.sh "${@}" -b install:install
done

vagrant global-status


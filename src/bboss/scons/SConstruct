
# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

def __targets_from_node(target_node):
    """
    Generate Scons targets from a bboss directory node.
    """
    builder = env_main.__dict__.get(target_node.context.builder)
    return builder(str(target_node), [], bboss_node=target_node)

def source_emitter(target, source, env):
    """
    SCons emitter used to generate the list of source files for the specified
    target.
    (https://www.scons.org/doc/production/HTML/scons-user.html#chap-builders-writing)

    target:
        List containing a single SCons Node object representing the target to
        be built.
    source:
        An empty list of source files, the source file list will be generated 
        by the emitter with the Bboss project's target node stored into 
        env["bboss_node"].
    env:
        The SCons construction environment used to build the target.
        An OverrideEnvironment which must contain a ["bboss_node"] variable.
        The Bboss node variable contains the target node in the Bboss project.
    """
    assert(len(target) == 1)
    assert(len(source) == 0)

    # Declare and attach source files to the target
    trg_node = env['bboss_node']
    for src_node in trg_node.context.inputs:
        if src_node.context.builder:
            src_file = __targets_from_node(src_node)
        else:
            src_file = str(src_node) 
        source.append(src_file)

    # Declare and attach dependencies files to the target
    lst_depends = trg_node.context.depends
    if lst_depends:
        env.Depends(target[0], [str(node) for node in lst_depends])

    return target, source

def main():
    """
    SConstruct main function converting Bboss project into SCons target lists.
    """
    from bboss import builder
    from bboss.project import Project

    # In the emitter we need to reach builder wrapper methods, however the
    # OverrideEnvironment provided in the env parameter does not have it.
    # The global definition is here our solution to get it without walking 
    # into the '__subject' chain.
    global env_main

    # Create the list of needed builders
    builders = {
        'bboss_copyfile' : Builder(action = builder.bboss_copyfile,
                                   emitter = source_emitter),
        'bboss_jinjavar' : Builder(action = builder.bboss_jinjavar,
                                   emitter = source_emitter),
        'bboss_mergeall' : Builder(action = builder.bboss_mergeall,
                                   emitter = source_emitter),
        'bboss_registry' : Builder(action = builder.bboss_registry,
                                   emitter = source_emitter),
        'bboss_dockconf' : Builder(action = builder.bboss_dockconf,
                                   emitter = source_emitter),
        'bboss_dockproj' : Builder(action = builder.bboss_dockproj,
                                   emitter = source_emitter),
        }

    # Define minimal SCons environment with Bboss builders
    env_main = Environment(tools={}, variables={}, BUILDERS = builders)

    # Scan files depending on project configuration (.config)
    project = Project()
    project.scan()

    # Evaluate whole project target lists
    try:
        # Files into the output directory
        for file_node in project.dir_output.iter_leaf(2):
            __targets_from_node(file_node)
        # All dir_target subtree.
        for file_node in project.dir_target.iter_leaf():
            __targets_from_node(file_node)
    except Exception as e:
        _ = e
        result = 1
    else:
        result = 0

    return result

if not GetOption('help'):
    main()

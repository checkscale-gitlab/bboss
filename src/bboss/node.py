
# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

import anytree

class NodeContext(object):
    def __init__(self, **kwargs):
        """
        The Node context is a safe place holder to keep specific node's 
        variables.

        Class defined variables are always availables from the object even not 
        initialized into the instance. If the variable is not overriden in the 
        object the call will return the default value from the class without 
        throwing exceptions.

        kwargs:
            Dictionary of variables to add to the object.
        """
        self.update(**kwargs)
        return

    def __getitem__(self, key):
        if key in self.__dict__: 
            return self.__dict__[key]

        cls_dict = self.__class__.__dict__
        if type(cls_dict.get(key, None)) is property:
            return cls_dict[key].__get__(self, key)

        return cls_dict[key]

    def __setitem__(self, key, value):
        obj_dict = self.__dict__
        cls_dict = self.__class__.__dict__
        # Let properties manage their own values
        if type(cls_dict.get(key, None)) is property:
            cls_dict[key].__set__(self, value)
        # Add the key, value if not already in self.__dict__
        elif key not in obj_dict:
            obj_dict[key] = value
        # Update existing key, value in self.__dict__
        elif type(obj_dict[key]) is list:
            if type(value) is list:
                obj_dict[key].extend(value)
            else:
                obj_dict[key].append(value)
        elif type(obj_dict[key]) is dict:
            obj_dict[key].update(value)
        else:
            obj_dict[key] = value
        return

    def __delitem__(self, key):
        if key in  self.__dict__:
            del self.__dict__[key]
        return

    def __contains__(self, key):
        return key in self.__dict__

    def __len__(self):
        return len(self.__dict__)

    @property
    def content_clone(self):
        """
        Returns a clone of the object's dictionary (self.__dict__) after 
        replacement of hidden instance variables by their related context 
        property's name.
        """
        retval = dict()
        for (key, value) in self.__dict__.items():
            if key[0] == '_' :
                key = key[1:]
            if type(value) in (list, dict):
                value = value.copy
            retval[key] = value
        return retval

    def keys(self):
        return self.__dict__.keys()

    def update(self, **kwargs):
        """
        Safely update the NodeContext, adding values to the lists, updating 
        dictionary and managing variables hidden by properties.

        kwargs:
            Dictionary of variables to add to the object or to update.
        """
        for key, value in kwargs.items():
            self[key] = value
        return

    def __str__(self):
        return "%s(%s)" % (self.__class__.__name__, self.__dict__)

class NodeBase(anytree.Node):
    """
    A simple tree node with a `name` and any `kwargs`.
    See : https://anytree.readthedocs.io/en/latest/

    Overridden here as a super class for RegistryNode and 
    DirectoryNode classes.
    """
    def add(self, path):
        """
        Create a node and all its parent nodes if needed, as mkdir -p does 
        for directories.

        path : 
            Path of the branch to add.
        Returns :
            Returns the last node created (leaf).
        """
        parent = self
        parent_class = type(self)
        
        # Remove empty leading and ending string in case of leading or ending
        # separator. Dot is also removed  specifically for the Direcroy 
        # implementation (relative path).
        keywords = path.split(self.separator)
        if keywords and keywords[0] == '.': keywords = keywords[1:]
        if keywords and not keywords[0]: keywords = keywords[1:]
        if keywords and not keywords[-1]: keywords = keywords[:-1]

        # Create a node for each keyword if needed
        for name in keywords:
            child = parent.get_child(name)
            if child is None:
                parent = parent_class(name, parent)
            else:
                parent = child 

        return parent

    def remove(self):
        """
        -p, --parents   remove DIRECTORY and its ancestors; e.g., 'rmdir -p a/b/c' is
                    similar to 'rmdir a/b/c a/b a'

        """
        child = self; parent = child.parent
        while (parent is not None) and (
            len(parent._NodeMixin__children) == 1):
            child.parent = None; child = parent; parent = child.parent
        child.parent = None
        return

    def get_child(self, name):
        """
        Returns the direct child corresponding to the given name or None if 
        there is None.
        Simple helper avoiding the need of a Resolver to find a direct child.
        """
        for child in self.children: 
            if child.name == name: return child
        return None

    def iter_all(self, maxlevel=None, fnfilter=None):
        """
        Iterate over this tree branch to yield all nodes.
        """
        return anytree.PreOrderIter(self, maxlevel=maxlevel, filter_=fnfilter)

    def iter_branch(self, maxlevel=None):
        """
        Iterate over this tree branch to only yield branch nodes.
        A branch node is a node with at least one child.
        """
        return anytree.PreOrderIter(self, filter_=lambda x: not x.is_leaf, 
                                    maxlevel=maxlevel)

    def iter_leaf(self, maxlevel=None):
        """
        Iterate over this tree branch to only yield leaf nodes (nodes 
        without child).
        """
        return anytree.PreOrderIter(self, filter_=lambda x: x.is_leaf,
                                    maxlevel=maxlevel)

    def __str__(self):
        """
        Returns the path of the current node without the leading separator.
        """
        return "%s" % self.separator.join(
            [str(node.name) for node in self.path])

class NodeResolver(anytree.Resolver):
    """
    Resolve :any:`Node` paths using attribute `pathattr`.
    See : https://anytree.readthedocs.io/en/latest/

    Overriden to implement an exception free finder.
    """
    def find(self, node, path):
        """
        Exception free finder returning node at path or None if path is not
        found.

        node
            Root node where searching must start.
        path
            Path of the Node to find relative to root node.
        """
        node, parts = self._Resolver__start(node, path)
        for part in parts:
            if part == "..":
                node = node.parent
            elif part in ("", "."):
                pass
            else:
                node = self._find(node, part)
                if node is None: return None
        return node

    def _find(self, node, name):
        for child in node.children:
            if getattr(child, self.pathattr) == name:
                return child
        return None

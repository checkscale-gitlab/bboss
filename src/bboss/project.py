
# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from os import path
from fnmatch import fnmatch
import itertools
import functools

from bboss.kconfigex import standard_kconfig_filter
from bboss.node import NodeContext, NodeBase, NodeResolver
from bboss.nodedir import DirectoryNode
from bboss.workspace import Workspace
from bboss.builder import _bboss_builders

resolver = NodeResolver()

class TargetContext(NodeContext):
    registry = None
    config   = None
#     build = None
#     create = None
#     all = None


class Project(NodeBase):
    """
    Represents the Bboss configuration used to build the infrastructure.
    
    The Project is a tree of nodes where each node must handle its 
    children, for instance :
    
    bboss     (Project)
        gitlab       (Package)
            gitlab         (Main service)
    
        proxy        (Package)
            nginx          (Main service)
    
        sonar        (Package)
            sonar          (Main service)
            sonardb        (Database Service)
    
    The Project instance also have 2 others tree of nodes :
    - RegistryNode
    - DirectoryNode
    
    The Registry is created during Configuration.__init__ and loaded with 
    values extracted from Kconfig and .config file.
    The Registry consider '_' as a node separator, creating a child each time 
    it found the separator in the variable name.
    """
    def __init__(self):
        # Load workspace and registry
        workspace = Workspace(False)
        self.registry = registry = workspace.open()
        self.name = registry['BLD_PROJECT_VARIANT']

        # Create the project virtual directory structure.
        self.dir_project = dir_project = DirectoryNode('.')
        self.dir_distrib = dir_distrib = dir_project.add(
            registry['BLD_SOURCE_DISTRIB'], project=self)
        self.dir_output  = dir_project.add(registry['BLD_OUTPUT_DIR_RELPATH'])
        self.dir_build   = dir_project.add(registry['BLD_OUTPUT_BLD_RELPATH'], 
                                           project=self)
        self.dir_target  = dir_project.add(registry['BLD_OUTPUT_TRG_RELPATH'])
        self.dep_config  = dir_project.add(workspace.filename)

        # Create project's expected targets.
        self.target = target = TargetContext()
        target.registry = self.dir_output.add('registry.lst', project=self,
                                              builder='bboss_registry',
                                              depends=[self.dep_config])
        target.config = self.dir_output.add('docker-compose.yml', project=self,
                                            builder='bboss_dockproj')

        # Create all declared packages with registry information.
        for reg_pkg in resolver.get(registry, 'PKG').children:
            Package(self, reg_pkg, dir_distrib, dir_project)

        # Inform all services on others existing services
        for srv in self.iter_services():
            srv._configure(self.iter_services(
                fnfilter=lambda node:node != srv))
        return


    def iter_packages(self, name_list=None, fnfilter=None):
        def _fnfilter(*argv, name_list=None, fnfilter=None):
            node=argv[0]
            if node.is_leaf or node.is_root: 
                return False
            if name_list and not node.name in name_list:
                return False
            return True if not fnfilter else fnfilter(node)
        return self.iter_all(fnfilter=functools.partial(
            _fnfilter, name_list=name_list, fnfilter=fnfilter))


    def iter_services(self, name_list=None, fnfilter=None):
        def _fnfilter(*argv, name_list=None, fnfilter=None):
            node=argv[0]
            if not node.is_leaf: 
                return False
            if name_list and not node.name in name_list:
                return False
            return True if not fnfilter else fnfilter(node)
        return self.iter_all(fnfilter=functools.partial(
            _fnfilter, name_list=name_list, fnfilter=fnfilter))


    def scan(self):
        # Package must scan its own files
        for pkg in self.children: 
            pkg._scan_files()
        return


class Package(NodeBase):
    """
    Represents a Bboss Package configuration, a package is a set of one or 
    several declared services providing a full functional feature.
    For intance nginx, is a single service package (nginx) and Sonarqube is a
    several services package (sonar & sonardb).

    Packages can contains several sub-directory for the same service 
    definition, these sub-directories must be mutually exclusive.
    For instance sonardb, the external SonarQube database is defined in 
    sonar.pggsl subdirectory (PostgreSQL definition) and in sonar.mysql 
    (MySQL definiton).
    Mutual exclusions are managed in Kconfig files by the menuconfig command 
    (Kconfiglib).
    """
    def __init__(self, parent, registry, dir_distrib, dir_source):
        super().__init__('unnamed', parent)
        self.registry = registry
        self.dep_config = parent.dep_config

        # Get required package information
        name      = registry['NAME']
        dir_path  = registry['RELPATH']
        pkg_cont  = registry['CONTENT']
        self.name = name

        # Create package's source virtual directory structure.
        self.dir_distrib = dir_distrib.add(dir_path)
        self.dir_source  = dir_source.add(dir_path, project=parent)

        # Create package's output virtual directory structure.
        self.dir_build  = parent.dir_build.add(name)
        self.dir_target = parent.dir_target

        # Create package's expected targets.
        self.target = target = TargetContext()
        target.registry = parent.target.registry
        target.config   = parent.target.config

        # Create package's services
        reg_inputs = target.registry.context.inputs
        cnf_inputs = target.config.context.inputs
        for reg_srv in [resolver.find(parent.registry, node) for 
                        node in pkg_cont.split()]:
            # Check if the service is available
            if not reg_srv: continue 

            # Create service
            srv = Service(self, reg_srv, self.dir_distrib, self.dir_source)

            # Add inputs to project's registry target
            if srv.compose == 'none': continue
            dir_target = srv.dir_target
            srv_target = dir_target.add('service-compose.yml',
                                        srv_current=srv,
                                        builder='bboss_dockconf',
                                        depends=[self.dep_config])
            reg_inputs.append(srv_target)

            # Add inputs to project's config target
            cnf_inputs.append(srv_target)
            cnf_inputs.append(dir_target.add('project-compose.yml', 
                                             srv_current=srv,
                                             builder='bboss_dockconf'))

        # Remove empty package
        if self.is_leaf:
            for obj in [
                self.dir_build, 
                self.dir_source, 
                self.dir_distrib, 
                self]: obj.parent = None
        return


    def _scan_files(self):
        # Service must scan its own files
        for srv in self.children: 
            srv._scan_files()

        # Clean unused package directories
        pkg_dir = self.dir_source
        if pkg_dir.is_leaf:
            pkg_dir.remove()
            self.dir_source = None

class Service(NodeBase):
    """
    Represents a Bboss Service configuration.
    Bboss service is a Docker service as it is defined in docker-compose.yml.
    """
    def __init__(self, parent, registry, dir_distrib, dir_source):
        super().__init__('unnamed', parent)
        self.target = parent.target
        self.registry = registry

        # Get required service information
        name         = registry['HOSTNAME']
        dir_path     = registry['RELPATH']
        self.compose = registry['COMPOSE']
        self.name    = name

        # Create node's source virtual directory structure.
        dir_oodt = dir_source if path.exists(
            path.join(str(dir_source), dir_path)) else dir_distrib
        self.dir_oodt = dir_oodt.add(dir_path, srv_current=self)

        # Create node's output virtual directory structure.
        self.dir_build  = parent.dir_build.add(name, srv_current=self)
        self.dir_target = parent.dir_target.add(name)

        return


    def _add_build(self, dir_source, file_path):
        '''
        Add file to the virtual directory tree into the dir_source branch and 
        the corresponding build directory branch.
        Override existing build file in the build directory if it already
        exists.

        dir_source:
            Node of the service source directory into the virtual directory 
            tree.
        filepah:
            File pathname relative to the given dir_source
        '''
        # Add the file to the virtual source directory tree
        file_source  = dir_source.add(file_path)

        # Adjust build file information depending on file extension
        file_name, file_ext = path.splitext(file_path)
        if file_ext == '.jin':
            if fnmatch(file_path, 'service-compose.yml.*'):
                file_deps = self.root.dep_config
            else:
                file_deps = self.target.registry
            file_path = file_name
            file_context = {'builder': _bboss_builders[file_ext], 
                            'depends': [file_deps]}
        else:
            file_context = {'builder': _bboss_builders['.*'],
                            'heritage': False}

        # Create virtual build directory and add the build target if it does
        # not exist or override the existing.
        file_context['inputs'] = [file_source]
        file_context['override'] = True
        self.dir_build.add(file_path, **file_context)
        return


    def _add_target(self, dir_name, file_build):
        # Remove dir_name's extension to allow configuration directory merge
        # into the target directory.
        dir_name, _ = path.splitext(dir_name)
        file_name = str(file_build)

        # Adjust target file information depending on file extension
        file_base, file_ext = path.splitext(file_name)
        if file_ext == '.add': file_name = file_base

        # If the built file is copied from the source, remove it and use the 
        # source file instead (pass-through)
        builder_copy = 'bboss_copyfile'
        if file_build.context.builder == builder_copy:
            file_node = file_build.context.inputs[0]
            file_build.remove()
        else:
            file_node = file_build

        # Create virtual target directory and add the build target if it does
        # not exist or update the existing.
        target = self.dir_target.add(path.join(dir_name, file_name), 
                                     inputs=[file_node])

        # If there is no builder, identify the builder (target creation)
        if not target.context.builder:
            if file_name in ('service-compose.yml', 'project-compose.yml'):
                target.context.builder = 'bboss_dockconf'
            else:
                target.context.builder = builder_copy
        # The target already existed, merge input files (target update)
        elif target.context.builder == builder_copy:
            target.context.builder = 'bboss_mergeall'
        return


    def _configure(self, srvlst):
        dir_list = ('rootfs','restapi')
        res_list = [DirectoryNode(str_dir) for str_dir in dir_list]
        res_list += [
            DirectoryNode("{}.{}".format(str_dir, srv.name), srv_config=srv)
            for str_dir, srv in list(
                itertools.product(dir_list, srvlst))]
        self.resources = res_list
        return


    def _scan_files(self):
        # Create directory node tree for OODT class hierarchy
        oodt_node = self.dir_oodt
        if not path.exists(str(oodt_node)): return
        oodt_lst = [oodt_node]
        oodt_dir  = '.super'
        while path.exists(path.join(str(oodt_node), oodt_dir)):
            oodt_node = oodt_node.add(oodt_dir)
            oodt_lst.insert(0, oodt_node)

        # Scan all OODT source expected physical directories and select most
        # appropriate files for the build directory.
        res_list = self.resources
        for srv_dir in oodt_lst:
            # Enter into the service OODT source directory
            str_dircwd = os.getcwd()
            os.chdir(str(srv_dir))

            # Add files found in expected resource sub-directories
            for res_dir in res_list:
                res_name = res_dir.name
                if not path.exists(res_name): continue
                res_dir = srv_dir.add(res_name, **res_dir.context.content_clone)
                for dirpath, _, filenames in os.walk(res_name, followlinks=True):
                    for filename in sorted(filenames):
                        self._add_build(srv_dir, path.join(dirpath, filename))
                if res_dir.is_leaf: res_dir.parent = None

            # Add service source files
            if self.compose != 'none':
                for entry in os.scandir('.'):
                    if not entry.is_file():
                        continue
                    if fnmatch(entry.name, standard_kconfig_filter):
                        continue
                    self._add_build(srv_dir, entry.name)

            # Back to the project directory.
            os.chdir(str_dircwd)

            # Remove service directory source node if it is empty.
            if srv_dir.is_leaf:
                srv_dir.parent = None

        # Scan virtual build directories and create virtual targets.
        for root_node in self.dir_build.children:
            # Check if the service directory's child node is a file.
            root_name = root_node.name
            if root_node.is_leaf:
                root_node.parent = None
                self._add_target('', root_node)
                root_node.parent = self.dir_build
                continue
            # Walk into the service build subdirectory node
            for dir_build in root_node.children:
                dir_build.parent = None
                for file_build in dir_build.iter_leaf():
                    self._add_target(root_name, file_build)
                if not dir_build.is_leaf:
                    dir_build.parent = root_node
            if root_node.is_leaf:
                root_node.parent = None
        if self.dir_build.is_leaf:
            self.dir_build.parent = None
            self.dir_build = None
        return

if __name__ == "__main__":
    from bboss.nodereg import RegistryRender
    from bboss.nodedir import DirectoryRender
    bboss_project = Project()
    #print('\n'.join([str(node) for node in bboss_project.iter_leaf()]))
    
#    print(RegistryRender(bboss_project.registry).list)
#    print(DirectoryRender(bboss_project.dir_project).dump)
    
    bboss_project.scan()
    print(DirectoryRender(bboss_project.dir_project).dump)
#    print(DirectoryRender(bboss_project.dir_output).dump)
#    print(DirectoryRender(bboss_project.dir_output).dump)
    pass
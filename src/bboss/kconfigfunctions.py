
# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

from os import path

def find_file(kconf, name, obj_path, file_path):
    # kconf:
    #   Kconfig instance
    #
    # name:
    #   Name of the user-defined function ("my-fn"). Think argv[0].
    #
    # arg_1, arg_2, ...:
    #   Arguments passed to the function from Kconfig (strings)
    #
    # Returns a string to be substituted as the result of calling the
    # function

    # Try to locate the file into obj_path or its .super sub-directories
    cur_dir = obj_path
    while(path.exists(cur_dir)):
        cur_path = path.join(cur_dir, file_path)
        if path.exists(cur_path): return cur_path
        cur_dir = path.join(cur_dir, '.super')

    # If the file is not found, return the searched file path.
    return path.join(obj_path, file_path)

def iif(kconf, name, expr, result_true, result_false):
    if eval(expr): return result_true
    return result_false

functions = {
    'find_file':    (find_file, 2, 2),
    'iif':          (iif,       3, 3),
}


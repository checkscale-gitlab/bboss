
# bboss Copyright (C) 2019, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

""" Current version of the bboss application.

This project uses the Semantic Versioning scheme in conjunction with PEP 0440:

    <http://semver.org/>
    <https://www.python.org/dev/peps/pep-0440>

Major versions introduce significant changes to the API, and backwards 
compatibility is not guaranteed. Minor versions are for new features and other
backwards-compatible changes to the API. Patch versions are for bug fixes and
internal code changes that do not affect the API. Development versions are
incomplete states of a release.

Version 0.x should be considered a development version with an unstable API,
and backwards compatibility is not guaranteed for minor versions.

"""
__description__  = 'Configure and run Bboss multi-container infrastructure '\
                    'project'
__author__       = 'Laurent Marchelli'
__author_email__ = 'laurent.marchelli@gmail.com'
__license__      = 'GNU General Public License v3'
__url__          = 'https://gitlab.com/bboss-org/bboss/'
__version__      = '0.0.1.dev18'


# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

def parse_args(argv):
    from inspect import getdoc
    from docopt import docopt
    from docopt import DocoptExit
    from .__version__ import __version__

    main_doc = getdoc(cli_main)
    main_opt = docopt(main_doc, argv, help=False, version=__version__, 
                      options_first=True)
    main_cmd = main_opt.get('COMMAND')

    # Check if there is a command, to get appropriate function
    if main_cmd is None:
        if main_opt['--help']: 
            raise SystemExit(main_doc)
        else:
            raise DocoptExit('[ERROR] No command specified')

    cmd_fnc = globals().get('cli_' + main_cmd)
    if cmd_fnc is None:
        raise DocoptExit('[ERROR] Unknown command : %s' % main_cmd)

    # We have a command specified with a valid command function.
    cmd_doc = getdoc(cmd_fnc)
    if cmd_doc is None:
        raise DocoptExit('Sorry, command not supported yet.')

    if main_opt['--help']:
        raise SystemExit(cmd_doc)

    cmd_opt = docopt(cmd_doc, argv, options_first=False)
    return (cmd_fnc, main_cmd, cmd_opt)


def cli_main():
    """
     
    Configure and run Bboss multi-container infrastructure project.
    
    Usage:
      bboss COMMAND [ARGS...]
      bboss -h [COMMAND]
      bboss -v
    
    Options:
      -h, --help        This screen if no command is specified, command help 
                        otherwise.
      -v, --version     bboss version.
    
    Commands:
      generate      Generate, build and start a new project from scratch.
      menuconfig    Run the menu to customize the configuration to your own
                    needs.
      config        Validate and display the docker-compose file generated.
      compose       Forward the command line to docker-compose with project
                    informations.
    
      build         Build service docker images.
      create        Create service containers.
      start         Start existing containers
      stop          Stop running containers
      logs          View output from services
    
      up            build + create + start.
      down          stop + remove containers + remove network
    
      lsi           List images.
      lsc           List containers.
      lsn           List networks.
      lsv           List volumes.
      lso           List objects. (images + containers + networks + volumes)
    
      rmi           Remove images.
      rmc           Remove containers.
      rmn           Remove networks.
      rmv           Remove volumes.
      rmo           Remove objects. (images + containers + networks + volumes)
      
      clean         down + erase docker images and buildchain outputs.
      distclean     clean + erase configuration (.config) and docker volumes.
    
    Arguments:
      ARGS          Command specific options and arguments.
     
    """
    import sys
    run_command(*parse_args(sys.argv[1:]))


def cli_generate(context):
    """
     
    Generate a new Bboss workspace from scratch.
    Display the configuration menu, build and start the project if specified.
    
    Usage:
      bboss generate [-q,-u] PROJECT
      bboss generate -h
    
    Options:
      -q, --quiet   Do not display the configuration menu.
      -u, --up      Build and start the project when the configuration is done.
      -h, --help    Show this screen.
    
    Arguments:
      PROJECT       Project's FQDN. 
                    If PROJECT does not contains a dot, the parameter is 
                    assumed to be the project name only, and the build machine 
                    domain will be used to create the project's FQDN.
                    If PROJECT contains a dot, PROJECT is the FQDN and the 
                    FQDN's hostname is the project name.
                    The command creates a git repository with the project's 
                    name to track project's files modifications.
     
    """
    if not context.options['--up']: 
        context.cmd_compose = None
    else:
        context.cmd_compose = ['up']
        context.options['--force'] = True
        cli_up(context)
    context.runmenu = not context.options['--quiet']


def cli_menuconfig(context):
    """
     
    Display the configuration menu to change project options, validate and
    display the docker-compose file generated.
    
    Usage:
      bboss menuconfig [-q]
      bboss menuconfig -h
    
    Options:
      -q, --quiet   Do not display the docker-compose file generated.
      -h, --help    Show this screen.
     
    """
    context.cmd_compose = ['config']
    if context.options['--quiet']: 
        context.cmd_compose.append('--quiet')
    context.runmenu = True


def cli_config(context):
    """
     
    Validate and display the docker-compose file generated.
    
    Usage:
      bboss config [-q]
      bboss config -h
    
    Options:
      -q, --quiet   Do not display the docker-compose file generated.
      -h, --help    Show this screen.
     
    """
    if context.options['--quiet']: 
        context.cmd_compose.append('--quiet')


def cli_compose(context):
    """
     
    Forward the command line to docker-compose with project's informations.
    
    Usage:
      bboss compose [options] COMMAND
      bboss compose [options] -- COMMAND [ARGS...]
      bboss compose -h
    
    Options:
      --verbose                   Show more output
      --log-level LEVEL           Set log level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
      --no-ansi                   Do not print ANSI control characters
      -H, --host HOST             Daemon socket to connect to
      -h, --help                  Show this screen.
    
      --tls                       Use TLS; implied by --tlsverify
      --tlscacert CA_PATH         Trust certs signed only by this CA
      --tlscert CLIENT_CERT_PATH  Path to TLS certificate file
      --tlskey TLS_KEY_PATH       Path to TLS key file
      --tlsverify                 Use TLS and verify the remote
      --skip-hostname-check       Don't check the daemon's hostname against the
                                  name specified in the client certificate
      --compatibility             If set, Compose will attempt to convert deploy
                                  keys in v3 files to their non-Swarm equivalent
    
    Commands:
      build              Build or rebuild services
      bundle             Generate a Docker bundle from the Compose file
      config             Validate and view the Compose file
      create             Create services
      down               Stop and remove containers, networks, images, and volumes
      events             Receive real time events from containers
      exec               Execute a command in a running container
      help               Get help on a command
      images             List images
      kill               Kill containers
      logs               View output from containers
      pause              Pause services
      port               Print the public port for a port binding
      ps                 List containers
      pull               Pull service images
      push               Push service images
      restart            Restart services
      rm                 Remove stopped containers
      run                Run a one-off command
      scale              Set number of containers for a service
      start              Start services
      stop               Stop services
      top                Display the running processes
      unpause            Unpause services
      up                 Create and start containers
      version            Show the Docker-Compose version information
     
    """
    cmd_args = [context.options['COMMAND']]
    cmd_args.extend(context.options['ARGS'])
    for o in ('compose', '--', 'COMMAND', 'ARGS'):
        del context.options[o]
    options = []
    for k, v in context.options.items():
        if not v: continue
        if v is True: options.append(k)
        else: options.extend((k, v))
    context.cmd_compose = options + cmd_args


def cli_build(context):
    """
     
    Build or rebuild docker images referenced into the docker-compose file
    generated.
    
    Usage: 
      bboss build [-l] [SERVICE...]
      bboss build -h
    
    Options:
      -l, --parallel    Build images in parallel.
      -h, --help        Show this screen.
     
    """
    options = context.cmd_compose
    if context.options['--parallel']: 
        options.append('--parallel')
    options.extend(context.options['SERVICE'])


def cli_create(context):
    """
     
    Creates docker containers referenced into the docker-compose file
    generated.
    
    Usage: 
      bboss create [-f] [SERVICE...]
      bboss create -h
    
    Options:
      -f, --force   Recreate containers even if their configuration and image
                    haven't changed.
      -h, --help    Show this screen.
     
    """
    # --no-build : Don't build an image, even if it's missing.
    # --no-start : Don't start the services after creating them.
    options = context.cmd_compose = ['up', '--no-build', '--no-start']
    if context.options['--force']: 
        options.append('--force-recreate')
    options.extend(context.options['SERVICE'])


def cli_start(context):
    """
     
    Start existing containers referenced into the docker-compose file 
    generated.
    
    Usage:
      bboss start [SERVICE...]
      bboss start -h
    
    Options:
      -h, --help        Show this screen.
     
    """
    context.cmd_compose.extend(context.options['SERVICE'])


def cli_stop(context):
    """
     
    Stop running containers referenced into the docker-compose file 
    generated.
    They can be started again with `bboss start`.
    
    Usage: 
      bboss stop [SERVICE...]
      bboss stop -h
    
    Options:
      -h, --help        Show this screen.
     
    """
    context.cmd_compose.extend(context.options['SERVICE'])


def cli_logs(context):
    """
     
    View output from container referenced into the docker-compose file 
    generated.
    
    Usage: 
      bboss logs [options] [SERVICE...]
      bboss logs -h
    
    Options:
      --no-color          Produce monochrome output.
      -f, --follow        Follow log output.
      -t, --timestamps    Show timestamps.
      --tail="all"        Number of lines to show from the end of the logs for 
                          each container.
      -h, --help          Show this screen.
     
    """
    options = context.cmd_compose
    services = context.options.pop('SERVICE')
    del context.options[options[0]]
    for k, v in context.options.items():
        if not v: continue
        if v is True: options.append(k)
        else: options.extend((k, v))
    options.extend(services)


def cli_up(context):
    """
     
    Builds docker images, creates and starts containers referenced into the
    docker-compose file generated.
    
    Usage: 
      bboss up [-f] [SERVICE...]
      bboss up -h
    
    Options:
      -f, --force   Recreate containers even if their configuration and image
                    haven't changed.
      -h, --help    Show this screen.
     
    """
    options = context.cmd_compose
    if context.options['--force']: 
        options.append('--force-recreate')
    # -d, --detach : Detached mode: Run containers in the background. 
    options.extend(('-d', '--build'))
    options.extend(context.options['SERVICE'])


def cli_down(context):
    """
     
    Stops and removes containers and networks referenced into the 
    docker-compose file generated.
    
    Usage: 
      bboss down [options]
      bboss down -h
    
    Options:
      -v, --volumes   Remove named volumes declared in the `volumes` section 
                      of the docker-compose file generated and anonymous volumes
                      attached to containers.
      -h, --help      Show this screen.
     
    """
    options = context.cmd_compose
    del context.options[options[0]]
    for k, v in context.options.items():
        if not v: continue
        if v is True: options.append(k)
        else: options.extend((k, v))


def cli_lsi(context):
    """
     
    List all images matching the filter or created by the current project's 
    variant in alphabetical order.
    
    Usage: 
      bboss lsi [-q] FILTER
      bboss lsi [-q]
      bboss lsi -h
    
    Options:
      -q, --quiet   Only display the list of images id.
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the image list with the specified value.
                    The filter follows the glob pattern convention.
                    Glob  asterisk wildcard is assumed at both side of the 
                    filter to make it smarter.
                    'bboss' is assumed to be '*bboss*'
    
    Example, try:
      bboss lsi             Display all images made by the current project's
                            variant.
      bboss lsi /           Display all images.
      bboss lsi /*:latest   Display all images with the latest tag.
      bboss lsi bboss       Display all images with a project's variant name
                            (organization) containing 'bboss'.
      bboss lsi dev/        Display all images with a project's variant name
                            (organization) ending with 'dev'.
      bboss lsi /sentry     Display all images with a service name starting
                            with 'sentry'.
      bboss lsi /*sentry    Display all images with a service name containing
                            'sentry'.
      bboss lsi /*sentry:   Display all images with a service name ending 
                            with 'sentry'.
      bboss lsi dev/sentry*:latest  
                            Display all images with a project's variant name 
                            (organization) ending with 'dev', a service name 
                            starting with 'sentry' and having the 'latest' tag.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_lsc(context):
    """
     
    List all containers matching the filter or created by the current project's 
    variant in alphabetical order.
    
    Usage: 
      bboss lsc [-q] FILTER
      bboss lsc [-q]
      bboss lsc -h
    
    Options:
      -q, --quiet   Only display the list of container id.
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the container list with the specified value.
                    The filter follows the regex pattern convention.
    
    Example, try:
      bboss lsc             Display all containers made by the current project's
                            variant.
      bboss lsc /           Display all containers with a 'normalized name'
                            (projectvariant_servicename)
      bboss lsc bboss       Display all containers with a name containing 
                            'bboss'.
      bboss lsc dev/        Display all containers with a project's variant name
                            (organization) ending with 'dev'.
      bboss lsc /sentry     Display all containers with a service name starting
                            with 'sentry'.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_lsn(context):
    """
     
    List all networks matching the filter or created by the current project's 
    variant in alphabetical order.
    
    Usage: 
      bboss lsn [-q] FILTER
      bboss lsn [-q]
      bboss lsn -h
    
    Options:
      -q, --quiet   Only display the list of network id.
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the network list with the specified value.
                    The filter follows the regex pattern convention.
    
    Example, try:
      bboss lsn         Display all networks made by the current project's
                        variant.
      bboss lsn /       Display all networks.
      bboss lsn bboss   Display all networks with a name containing 'bboss'.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_lsv(context):
    """
     
    List all volumes matching the filter or created by the current project's 
    variant in alphabetical order.
    
    Usage: 
      bboss lsv [-q] FILTER
      bboss lsv [-q]
      bboss lsv -h
    
    Options:
      -q, --quiet   Only display the list of volume id.
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the volume list with the specified value.
                    The filter follows the regex pattern convention.
    
    Example, try:
      bboss lsv         Display all volumes made by the current project's
                        variant.
      bboss lsv /       Display all volumes.
      bboss lsv bboss   Display all volumes with a name containing 'bboss'.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_lso(context):
    """
     
    List all objects matching the filter or created by the current project's 
    variant in alphabetical order. (images + containers + networks + volumes)
    
    Usage: 
      bboss lso FILTER
      bboss lso
      bboss lso -h
    
    Options:
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the object list with the specified value.
    
    Example, try:
      bboss lso         Display all objects made by the current project's
                        variant.
      bboss lso /       Display all objects.
      bboss lso bboss   Display all objects with a name containing 'bboss'.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_rmi(context):
    """
     
    Remove all images matching the filter or created by the current project's 
    variant.
    
    Usage: 
      bboss rmi FILTER
      bboss rmi
      bboss rmi -h
    
    Options:
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the image list with the specified value.
                    The filter follows the glob pattern convention.
                    Glob  asterisk wildcard is assumed at both side of the 
                    filter to make it smarter.
                    'bboss' is assumed to be '*bboss*'
    
    Example, try:
      bboss rmi             Remove all images made by the current project's
                            variant.
      bboss rmi /           Remove all images.
      bboss rmi /*:latest   Remove all images with the latest tag.
      bboss rmi bboss       Remove all images with a project's variant name
                            (organization) containing 'bboss'.
      bboss rmi dev/        Remove all images with a project's variant name
                            (organization) ending with 'dev'.
      bboss rmi /sentry     Remove all images with a service name starting
                            with 'sentry'.
      bboss rmi /*sentry    Remove all images with a service name containing
                            'sentry'.
      bboss rmi /*sentry:   Remove all images with a service name ending 
                            with 'sentry'.
      bboss rmi dev/sentry*:latest  
                            Remove all images with a project's variant name 
                            (organization) ending with 'dev', a service name 
                            starting with 'sentry' and having the 'latest' tag.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_rmc(context):
    """
     
    Remove all containers matching the filter or created by the current 
    project's variant.
    
    Usage: 
      bboss rmc FILTER
      bboss rmc
      bboss rmc -h
    
    Options:
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the container list with the specified value.
                    The filter follows the regex pattern convention.
    
    Example, try:
      bboss rmc             Remove all containers made by the current project's
                            variant.
      bboss rmc /           Remove all containers with a 'normalized name'
                            (projectvariant_servicename)
      bboss rmc bboss       Remove all containers with a name containing 
                            'bboss'.
      bboss rmc dev/        Remove all containers with a project's variant name
                            (organization) ending with 'dev'.
      bboss rmc /sentry     Remove all containers with a service name starting
                            with 'sentry'.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_rmn(context):
    """
     
    Remove all networks matching the filter or created by the current
    project's variant.
    
    Usage: 
      bboss rmn FILTER
      bboss rmn
      bboss rmn -h
    
    Options:
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the network list with the specified value.
                    The filter follows the regex pattern convention.
    
    Example, try:
      bboss rmn         Remove all networks made by the current project's
                        variant.
      bboss rmn /       Remove all networks.
      bboss rmn bboss   Remove all networks with a name containing 'bboss'.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_rmv(context):
    """
     
    Remove all volumes matching the filter or created by the current
    project's variant.
    
    Usage: 
      bboss rmv FILTER
      bboss rmv
      bboss rmv -h
    
    Options:
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the volume list with the specified value.
                    The filter follows the regex pattern convention.
    
    Example, try:
      bboss rmv         Remove all volumes made by the current project's
                        variant.
      bboss rmv /       Remove all volumes.
      bboss rmv bboss   Remove all volumes with a name containing 'bboss'.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_rmo(context):
    """
     
    Remove all objects matching the filter or created by the current
    project's variant. (images + containers + networks + volumes)
    
    Usage: 
      bboss rmo FILTER
      bboss rmo
      bboss rmo -h
    
    Options:
      -h, --help    Show this screen.
    
    Arguments:
      FILTER        Filter the object list with the specified value.
    
    Example, try:
      bboss rmo         Remove all objects made by the current project's
                        variant.
      bboss rmo /       Remove all objects.
      bboss rmo bboss   Remove all objects with a name containing 'bboss'.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_clean(context):
    """
     
    Stops and clean project containers, images and output generated.
    (down + erase docker images and buildchain outputs)
    
    Usage: 
      bboss clean
      bboss clean -h
    
    Options:
      -h, --help    Show this screen.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def cli_distclean(context):
    """
     
    Do clean and remove configuration and user data generated.
    (clean + erase configuration and docker volumes)
    
    Usage: 
      bboss distclean
      bboss distclean -h
    
    Options:
      -h, --help    Show this screen.
     
    """
    context.cmd_wkspace = context.cmd_compose
    context.cmd_compose = None
    context.runmenu = False


def run_command(function, command, options):
    import os
    import sys
    import subprocess

    from compose.cli.main import main

    from bboss.node import NodeContext
    from bboss.workspace import workspace_generate, Workspace

    # Let the command handler override the context if needed
    class CommandContext(NodeContext):
        cmd_compose = None
        cmd_wkspace = None
        options = None
        runmenu = None
    context = CommandContext(cmd_compose=[command], options=options)
    function(context)

    # Generate the workspace if required
    if command == 'generate':
        workspace = workspace_generate(context.options['PROJECT'], 
                                      context.runmenu)
        registry = workspace.registry
    # Open the workspace and show menu if requested
    else:
        workspace = Workspace(context.runmenu)
        registry = workspace.open(True)

    if context.cmd_wkspace:
        fn = getattr(Workspace, context.cmd_wkspace[0])
        fn(workspace, **context.options)

    if context.cmd_compose:
        # Run scons to prepare configuration files
        list_args = ['scons', '--sconstruct', os.path.join(
            os.path.dirname(__file__), 'scons', 'SConstruct')]
        print(" ".join(list_args))
        subprocess.run(list_args).check_returncode()

        # Run docker-compose to manage containers
        list_args = [
            'docker-compose', '-p', workspace.variant, 
            '-f', os.path.join(registry['BLD_OUTPUT_DIR_RELPATH'],
                'docker-compose.yml'),
        ]
        list_args += context.cmd_compose
        print(" ".join(list_args))
        sys.argv = list_args; main()


if __name__ == "__main__":
    cli_main()

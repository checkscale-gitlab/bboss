
# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from os import path

import git
from bboss.kconfigex import standard_kconfig_filename

# Change current directory to bboss workspace's root directory
def chdir2root():
    try:
        while(True):
            prj_repo = git.Repo(search_parent_directories=True)
            # Look for the distribution submodule
            if len(prj_repo.heads) or prj_repo.head.is_detached:
                prj_dist = getattr(prj_repo.submodules, '.distrib', None)
            else: 
                prj_dist = None
            # Go to the current repository's root directory
            os.chdir(prj_repo.working_dir)
            # If we found the distribution, we are at the right place.
            if prj_dist is not None: 
                break
            os.chdir('..')
        # Check bboss distribution
        if not path.exists(standard_kconfig_filename):
            raise Exception()
        if not path.exists(
            path.join(prj_dist.path, standard_kconfig_filename)):
            raise Exception()
    except Exception:
        raise SystemExit(
            '[ERROR] You are not in a bboss workspace, please enter ' \
            'into the workspace directory before running this command.')
    return


def human_readable(number, suffixes=(
        ('B', 0),('KB', 0),('MB', 1),('GB', 2),('TB', 2))):
    ndx = 0
    while number > 1000 and ndx < len(suffixes):
        ndx += 1
        number = number / 1000.0
    suffix, precision = suffixes[ndx]
    return "%.*f%s" % (precision, number, suffix)



# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

from anytree import RenderTree

from bboss.node import NodeContext, NodeBase

class DirectoryContext(NodeContext):
    """
    The Directory context is a safe place holder to keep specific Directory 
    node's variables.

    Class defined variables are always availables from the object even not 
    initialized into the instance. If the variable is not overriden in the 
    object the call will return the default value from the class without 
    throwing exceptions.

    For instance :
    >>> test = DirectoryContext()
    >>> print(test)
    DirectoryContext({})
    >>> print(test.srv_current)
    None
    >>> test.update(**{'srv_current':'bboss/gitlab/gitlab'})
    >>> print(test)
    DirectoryContext({'srv_current': 'bboss/gitlab/gitlab'})
    >>> print(test.srv_current)
    bboss/gitlab/gitlab

    kwargs:
        Dictionary of variables to add the object.
    """
    # Default file and directory properties
    project = None
    _srv_config = None
    srv_current = None

    # Default file context properties
    _builder = None
    depends = None
    inputs = None

    @property
    def builder(self):
        return self._builder
        
    @builder.setter
    def builder(self, builder):
        """
        Create an input list, even empty, as soon as the builder is set.
        """
        self._builder = builder
        if builder and 'inputs' not in self.__dict__:
            self.inputs = list()

    @property
    def srv_config(self):
        """ 
        Returns the service to configure for the current service.
        
        If there is no service to configue define in the instance, the service
        to configure is assumed to be the current service.
        """
        return self._srv_config if '_srv_config' in self.__dict__ \
            else self.srv_current

    @srv_config.setter
    def srv_config(self, value):
        """ 
        Set the service to configure for the current service.
        """
        self._srv_config = value
        return

class DirectoryNode(NodeBase):
    context = DirectoryContext()
    separator = "/"

    def __init__(self, name, parent=None, **kwargs):
        """
        A simple tree node with a `name` and any `kwargs`.
        See : https://anytree.readthedocs.io/en/latest/
        
        Overrided to manage DirectoryContext and DirectoryContext inheritance.

        name:
            Directory Node name (directory name or file name)
        parent:
            Node's parent (owner's directory)
        kwargs:
            Dictionary used to initialize the context, not the super class.
        """
        super().__init__(name, parent)
        # If you have a parent copy the parent context, if not and have
        # no kwargs, you don't need your own context.
        if (parent is not None) and (kwargs.pop('heritage', True)):
            kw = parent.context.__dict__
        elif kwargs:
            kw = self.context.__dict__
        else:
            return

        self.context = DirectoryContext(**kw)
        self.context.update(**kwargs)
        return

    def add(self, path, **kwargs):
        """
        Create a node and all its parent nodes if needed, as mkdir -p does for
        directories. Update leaf node's context with kwargs.

        path: 
            Path of the directory branch to add to this node.
        kwargs: 
            Dictionary used to update the leaf context.
        Returns:
            Returns the last node created of found (leaf).
        """
        node = super().add(path)
        if kwargs:
            context = node.__dict__.pop('context', None)
            override = kwargs.pop('override', False)
            heritage = kwargs.pop('heritage', True)
            if override or (context is None):
                if (node.parent is not None) and heritage:
                    kw = node.parent.context.__dict__
                else:
                    kw = node.context.__dict__
                context = DirectoryContext(**kw)
            context.update(**kwargs)
            node.context = context
        return node

class DirectoryRender(RenderTree):
    """
    Render Registry tree starting at given node.
    See : https://anytree.readthedocs.io/en/latest/

    Implemented to provide Directory smart output in tree shape with or 
    without DirectoryContext dump.
    """

    @property
    def dump(self):
        return self._dump(fnfilter=lambda node: not node.is_leaf)
        
    @property
    def dump_all(self):
        return self._dump()

    def _dump(self, fnfilter=None):
        """
        Returns the directory tree with the DirectoryContext dump.
        """
        if fnfilter is None:
            fnfilter = lambda _: False
        strResult = ""
        for pre, fill, node in self:
            strResult += "%s%s\n" % (pre, node.name)
            fill += " " * 4 if node.is_leaf else self.style.vertical
            # Dump the leaf node context if it exists
            if fnfilter(node): continue
            context = getattr(node, 'context', None)
            if context is None: continue
            lst_items = [(key[1:] if key.startswith('_',0,1) else key,
                          value) for (key, value) in context.__dict__.items()]
            for key, value  in sorted(lst_items):
                if (type(value) is list) and (len(value) > 0) :
                    if (len(value) == 1):
                        strResult += "%s%s = [%s]\n" % (fill, key, str(value[0]))
                    else:
                        # Print first Line
                        prefix = "%s = [" % (key)
                        strResult += "%s%s%s,\n" % (fill, prefix, value[0])
                        # Print middle Lines
                        prefix = " " * len(prefix)
                        for line in value[1:-1]:
                            strResult += "%s%s%s,\n" % (fill, prefix, line)
                        # Print last Lines
                        strResult += "%s%s%s]\n" % (fill, prefix, value[-1])
                else:
                    strResult += "%s%s = %s\n" % (fill, key, str(value))
            strResult += "%s\n" % (fill)
        return strResult

    def __str__(self):
        """
        Returns the directory tree without the leading /
        """
        return '\n'.join(
            ["%s%s" % (pre, node.name) for pre, _, node in self])


# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from os import path
from fnmatch import fnmatch
from shutil import rmtree

import git
import docker
from anytree.importer.dictimporter import DictImporter
from anytree.importer.jsonimporter import JsonImporter
from menuconfig import menuconfig
from terminaltables.base_table import BaseTable

from bboss.kconfigex import standard_config_filename, BOOL, STRING, INT, HEX, \
                            standard_kconfig_filename, KconfigEx
from bboss.nodereg import RegistryNode, NodeResolver
from bboss.utils import chdir2root, human_readable


standard_workspace_filename = '.workspace'

class Workspace(object):


    def __init__(self, showmenu, filename = standard_workspace_filename):
        # Check the current directory is inside a bboss workspace.
        chdir2root()

        # Start initialization
        self._docker = None
        self._table_format = None

        # If showmenu is not specified (None), show the menu if the '.config' 
        # file does not exist.
        self.showmenu = showmenu if showmenu is not None else \
            not path.exists(standard_config_filename())
        self.filename = filename
        self.kconfig = None
        self.registry = None
        self.variant = None

        # Load existing '.workspace' file (no modification expected)
        if not self.showmenu and path.exists(filename):
            jsn_imp = JsonImporter(dictimporter=DictImporter(RegistryNode))
            with open(filename, 'r') as file_in:
                self.registry = jsn_imp.read(file_in)
            self.variant = self.registry['BLD_PROJECT_VARIANT']


    def open(self, fastload=False):
        # Check if the registry is already loaded
        if self.registry:
            return self.registry

        os.environ['CONFIG_'] = 'BBOSS_'
        os.environ['KCONFIG_FUNCTIONS'] = 'bboss.kconfigfunctions'

        # User is not allowed to do modifications and we just need a reduced 
        # registry with global project's informations. (fast loading)
        fastload = fastload and not self.showmenu
        if fastload:
            os.environ['BBOSS_ENV_KCONFIG'] = 'None'
            kconfig = KconfigEx(standard_kconfig_filename)
            del os.environ['BBOSS_ENV_KCONFIG']
        # Load kconfig to be able to create or update '.workspace' after 
        # user's modifications.
        else:
            kconfig = KconfigEx(standard_kconfig_filename)
            if not path.exists(standard_config_filename()):
                kconfig.write_config()

        # Display Menu if required
        if self.showmenu:
            menuconfig(kconfig)

        # Create and fill up the registry with Kconfig and .config variables
        verbose = kconfig.load_config()
        if not self.showmenu and not fastload:
            print(verbose)
        registry = RegistryNode(str(kconfig.config_prefix)[:-1])
        for sym in kconfig.unique_defined_syms:
            name, value = sym.name, sym.str_value
            if (sym.orig_type == BOOL):
                if name.startswith('GUI_'): continue
                if (value != 'y'): continue
            elif sym.orig_type == STRING:
                if not value: continue
                value = kconfig.parse_symbol(sym)
                sym._cached_str_val = value
            elif sym.orig_type not in (INT, HEX): 
                continue
            registry.add(name, value)

        # Save only the complete registry to '.workspace' file
        if not fastload:
            registry.save_json(standard_workspace_filename)

        # Initialize remaining workspace variables
        self.kconfig  = kconfig
        self.registry = registry
        self.variant  = registry['BLD_PROJECT_VARIANT']
        return registry


    @property
    def docker(self):
        if not self._docker:
            self._docker = docker.from_env()
        return self._docker


    def get_image_filter(self, lsfilter=None):
        if lsfilter is None: 
            lsfilter = self.variant + "/*"
        elif '/' not in lsfilter:
            lsfilter = "*%s*/*" % lsfilter
        else:
            lsfilter = "*%s*" % lsfilter
        return lsfilter


    def get_images(self, lsfilter=None):
        lsfilter = self.get_image_filter(lsfilter)
        return self.docker.images.list(lsfilter)


    def get_networks(self, lsfilter=None):
        if lsfilter is None:
            lsfilter = self.variant
        else:
            if lsfilter.endswith('*'): lsfilter = lsfilter[0:-1]
            lsfilter = lsfilter.replace('/', '')
        return self.docker.networks.list(lsfilter)


    def get_containers(self, lsfilter=None):
        if lsfilter is None:
            lsfilter = self.variant
        else:
            lsfilter = '_'.join(lsfilter.split('/')[0:2])
        return self.docker.containers.list(all=True, filters={'name':lsfilter})


    def get_volumes(self, lsfilter=None):
        if lsfilter is None:
            lsfilter = self.variant
        else:
            if lsfilter.endswith('*'): lsfilter = lsfilter[0:-1]
            lsfilter = '_'.join(lsfilter.split('/')[0:2])
        return self.docker.volumes.list(filters={'name':lsfilter})


    @property
    def table_format(self):
        """
        Return the standard table formating object to print list of objects
        created by the project.
        """
        if not self._table_format:
            table = BaseTable(None)
            table.inner_column_border = False
            table.inner_footing_row_border = False
            table.inner_column_border = False
            table.inner_footing_row_border = False
            table.inner_heading_row_border = False
            table.inner_row_border = False
            table.outer_border = False
            table.padding_left = 0
            table.padding_right = 3
            self._table_format = table
        return self._table_format


    def lsi(self, **kwargs):
        """
        Display all images matching the filter or created by the current
        project's variant in alphabetical order.
        """
        if kwargs.get('--quiet', None):
            rows = [img.id[7:19] for img in 
                    self.get_images(kwargs.get('FILTER', None))]
            print(' '.join(sorted(rows)))
        else:
            # Output table rows
            rows = []
            img_flt = self.get_image_filter(kwargs.get('FILTER', None))
            for img in self.get_images(kwargs.get('FILTER', None)):
                for tag_str in img.tags:
                    if not fnmatch(tag_str, img_flt):
                        continue
                    tag_ndx = tag_str.rfind(':')
                    dte_str = img.attrs['Created'].replace('T', ' ')
                    dte_str = dte_str[:dte_str.rfind('.')]
                    rows.append((tag_str[:tag_ndx], tag_str[tag_ndx+1:],
                                 img.id[7:19], dte_str,
                                 human_readable(img.attrs['Size']),
                                 ))
            # Output table header
            rows.sort()
            rows.insert(0,('REPOSITORY', 'TAG',
                           'IMAGE ID', 'CREATED',
                           'SIZE'))
            # Draw table output
            table = self.table_format
            table.table_data = rows
            print(table.table)


    def lsn(self, **kwargs):
        """
        Display all networks matching the filter or created by the current
        project's variant in alphabetical order.
        """
        if kwargs.get('--quiet', None):
            rows = [net.id[:12] for net in 
                    self.get_networks(kwargs.get('FILTER', None))]
            print(' '.join(sorted(rows)))
        else:
            # Output table rows
            rows = []
            for net in self.get_networks(kwargs.get('FILTER', None)):
                dte_str = net.attrs['Created'].replace('T', ' ')
                dte_str = dte_str[:dte_str.rfind('.')]
                rows.append((net.name, net.id[:12], net.attrs['Driver'], 
                             net.attrs['Scope'], dte_str))
            # Output table header
            rows.sort()
            rows.insert(0,('NAME', 'NETWORK ID','DRIVER', 
                           'SCOPE', 'CREATED'))
            # Draw table output
            table = self.table_format
            table.table_data = rows
            print(table.table)


    def lsv(self, **kwargs):
        """
        Display all volumes matching the filter or created by the current
        project's variant in alphabetical order.
        """
        if kwargs.get('--quiet', None):
            rows = [vol.id for vol in 
                    self.get_volumes(kwargs.get('FILTER', None))]
            print(' '.join(sorted(rows)))
        else:
            # Output table rows
            rows = []
            for vol in self.get_volumes(kwargs.get('FILTER', None)):
                dte_str = vol.attrs['CreatedAt'].replace('T', ' ')
                dte_str = dte_str.split('+')[0]
                rows.append((vol.id, vol.attrs['Driver'], 
                             dte_str, vol.attrs['Mountpoint']))
            # Output table header
            rows.sort()
            rows.insert(0,('NAME', 'DRIVER', 
                           'CREATED', 'MOUNT POINT'))
            # Draw table output
            table = self.table_format
            table.table_data = rows
            print(table.table)


    def lsc(self, **kwargs):
        """
        Display all containers matching the filter or created by the current
        project's variant in alphabetical order.
        """
        if kwargs.get('--quiet', None):
            rows = [cnt.id[:12] for cnt in 
                    self.get_containers(kwargs.get('FILTER', None))]
            print(' '.join(sorted(rows)))
        else:
            # Output table rows
            rows = []
            img_flt = self.get_image_filter(kwargs.get('FILTER', None))
            for cnt in self.get_containers(kwargs.get('FILTER', None)):
                # Get the list of image tags
                try:
                    img_tags = cnt.image.tags
                except docker.errors.ImageNotFound:
                    img_tags = ['']
                # Try to reduce the number of choices with the container
                # filter
                if len(img_tags) > 1:
                    tags = [tag for tag in img_tags if fnmatch(tag, img_flt)]
                    if tags: img_tags = tags
                # Try to reduce the number of choices with docker-compose
                # informations
                if (len(img_tags) > 1) and (
                    'com.docker.compose.project' in cnt.labels):
                    tags = [tag for tag in img_tags if 
                            fnmatch(tag, "%s/%s:*" % (
                            cnt.labels['com.docker.compose.project'],
                            cnt.labels['com.docker.compose.service']))]
                    if tags: img_tags = tags
                # Try to reduce the number of choice with container name
                if len(img_tags) > 1:
                    tags = [tag for tag in img_tags if fnmatch(
                        tag, '/'.join(cnt.name.split('_')[0:2]))]
                    if tags: img_tags = tags
                # We have tried our best, get the first in the remaining list
                tag_str = img_tags[0]
                dte_str = cnt.attrs['Created'].replace('T', ' ')
                dte_str = dte_str[:dte_str.rfind('.')]
                arg_str = " ".join([cnt.attrs['Path']] + cnt.attrs['Args'])
                rows.append((
                    cnt.name, cnt.id[:12], tag_str, dte_str,
                    cnt.status, cnt.attrs['NetworkSettings']['Ports'],
                    "\"%s\"" % (arg_str)))
            # Output table header
            rows.sort()
            rows.insert(0,('NAME', 'CONTAINER ID','IMAGE','CREATED',
                           'STATUS','PORTS', 
                           'COMMAND'))
            # Draw table output
            table = self.table_format
            table.table_data = rows
            print(table.table)


    def lso(self, **kwargs):
        """
        Display all objects matching the filter or created by the current
        project's variant in alphabetical order.
        (images + networks + volumes + containers)
        """
        self.lsi(**kwargs); print('')
        self.lsn(**kwargs); print('')
        self.lsv(**kwargs); print('')
        self.lsc(**kwargs); print('')


    def rmi(self, **kwargs):
        """
        Remove all images matching the filter or created by the current
        project's variant.
        """
        image_lst = self.docker.images
        for img in self.get_images(kwargs.get('FILTER', None)):
            image_lst.remove(image=img.id, force=True)


    def rmn(self, **kwargs):
        """
        Remove all networks matching the filter or created by the current
        project's variant.
        """
        for net in self.get_networks(kwargs.get('FILTER', None)):
            net.remove()


    def rmv(self, **kwargs):
        """
        Remove all volumes matching the filter or created by the current
        project's variant.
        """
        for vol in self.get_volumes(kwargs.get('FILTER', None)):
            vol.remove(force=True)


    def rmc(self, **kwargs):
        """
        Remove all containers matching the filter or created by the current
        project's variant.
        """
        for cnt in self.get_containers(kwargs.get('FILTER', None)):
            cnt.remove(force=True)


    def rmo(self, **kwargs):
        """
        Remove all objects matching the filter or created by the current
        project's variant.
        """
        self.rmc(**kwargs)
        self.rmn(**kwargs)
        self.rmi(**kwargs)
        self.rmv(**kwargs)


    def clean(self, **kwargs):
        """
        Stops and clean project containers, images and output generated.
        (down + erase docker images and buildchain outputs)
        """
        # Remove docker objects
        self.rmc()
        self.rmn()
        self.rmi()
        # Remove build output directories
        registry = NodeResolver().get(self.registry, 'BLD_OUTPUT')
        for d in (registry['TRG_RELPATH'],
                  registry['BLD_RELPATH'],
                  registry['DIR_RELPATH']):
            if not path.exists(d): continue
            rmtree(d, ignore_errors=False, onerror=None)
        _ = kwargs


    def distclean(self, **kwargs):
        """
        Do clean and remove configuration and user data generated.
        (clean + erase configuration and docker volumes)
        """
        self.clean()
        self.rmv()
        # Remove .config and .workspace
        for f in (standard_workspace_filename, 
                  standard_config_filename()):
            if not path.exists(f): continue
            os.remove(f)
        _ = kwargs


text_kconfig="""
# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

bld_project_name  := {p[BLD_PROJECT_NAME]}
bld_project_netdn := {p[BLD_PROJECT_NETDN]}
bld_output_subdir := {p[BLD_OUTPUT_DIR_RELPATH]}

orsource '{p[BLD_SOURCE_DISTRIB]}/Kconfig.bboss'
"""


def workspace_generate(prj_fqdn, showmenu):
    dist_url    = os.getenv('BBOSS_ENV_DISTRIB_URL')
    dist_branch = os.getenv('BBOSS_ENV_DISTRIB_BRANCH')
    dist_subdir = os.getenv('BBOSS_ENV_DISTRIB_SUBDIR')
    bld_output  = os.getenv('BBOSS_ENV_OUTPUT_SUBDIR')

    # Just to be able to detect variables declared with an empty value
    # e.g. BBOSS_ENV_DISTRIB_BRANCH=''
    if not dist_url:
        dist_url = 'https://gitlab.com/bboss-org/bboss-distrib.git'
    if not dist_branch: dist_branch = 'release/0.x'
    if not dist_subdir: dist_subdir = '.distrib'
    if not bld_output:  bld_output = 'output'

    # Extract name and domain from given parameter
    prj_list    = prj_fqdn.split('.')
    prj_name    = prj_list[0] if prj_list[0] else 'bboss'
    prj_domain  = '.'.join(prj_list[1:])

    # Check the project's directory does not already exist
    if path.exists(prj_name):
        raise SystemExit(
            "[ERROR] A directory named '%s' already exists" % prj_name)

    # Check the current directory is not already inside an existing 
    # Git workspace.
    try:
        prj_repo = git.Repo(search_parent_directories=True)
    except git.InvalidGitRepositoryError:
        prj_repo = None

    if prj_repo is not None:
        raise SystemExit(
            "[ERROR] Unable to generate a workspace inside an existing " \
            "Git workspace.")

    # Create project's repository
    prj_repo = git.Repo.init(prj_name)
    prj_repo.create_submodule('.distrib', dist_subdir, dist_url, dist_branch)
    os.chdir(prj_name)
    file_list = ('.gitignore', standard_kconfig_filename)

    # Create .gitignore file
    with open(file_list[0], "w") as file_out:
        file_out.write("/%s/\n/.sconsign.dblite\n" % bld_output)

    # Create root Kconfig.bboss file
    params = {
        'BLD_PROJECT_NAME':         prj_name,
        'BLD_PROJECT_NETDN':        prj_domain,
        'BLD_OUTPUT_DIR_RELPATH':   bld_output,
        'BLD_SOURCE_DISTRIB':       dist_subdir
    }
    with open(file_list[1], "w") as file_out:
        file_out.write(text_kconfig.format(p=params))

    # Create initial commit
    prj_repo.index.add(file_list)
    prj_repo.index.commit("Bboss Initial commit")

    # Create .config and .registry files
    workspace = Workspace(showmenu)
    workspace.open()

    return workspace

if __name__ == "__main__":
    from bboss.nodereg import RegistryRender
    workspace = Workspace(False)
    workspace.open(True)
    print(RegistryRender(workspace.registry).list)

    workspace = Workspace(False)
    workspace.open(False)
    print(RegistryRender(workspace.registry).list)

